<?php
extract($_POST);
try
{
    require 'db.php';
    $login = $_POST['save'];
    $sth = $db->prepare("DELETE FROM anketa3 WHERE login=:login");
    $sth->bindParam(':login', $login);
    $sth->execute();
    header('Location: admin.php');
}
catch(PDOException $e)
{
    print ('Error : ' . $e->getMessage());
    exit();
}
